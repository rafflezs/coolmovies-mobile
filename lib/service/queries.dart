class Queries {
  String allMovies() {
    return """
    query MyQuery {
      allMovies {
        edges {
          node {
            id
            title
            releaseDate
            movieDirectorId
          }
        }
      }
    }
    """;
  }

  String createUser(String name) {
    return """
   mutation {
  createUser(input: {user: {name: "${name}"}}) {
    user {
      id
      name
    }
  }
}
  """;
  }

  String userReviews(String id) {
    return """
          query {
  movieReviewById(id: "${id}") {
    body
    id
    movieByMovieId {
      id
      releaseDate
      title
      movieDirectorByMovieDirectorId {
        age
        id
        name
      }
    }
    rating
    nodeId
    title
    userByUserReviewerId {
      name
      id
    }
  }
}
          """;
  }

  String allMovieReviews() {
    return """
    query {
  allMovieReviews {
    nodes {
      title
      body
      rating
      movieByMovieId {
        id
        title
        userByUserCreatorId {
          id
          name
        }
      }
      commentsByMovieReviewId {
        nodes {
          id
          title
          body
          userByUserId {
            id
            name
          }
        }
      }
    }
  }
}
    """;
  }

  String movieDirectorById(String id) {
    return """
    query {
	movieDirectorById (id : "${id}") {
			name,
			id	
	}
}
    """;
  }

  String reviewByMovieId(String id) {
    return """
    {
  allMovieReviews(
    filter: {movieId: {equalTo: "${id}"}}
  ) {
    nodes {
      title
      body
      rating
			id
			userReviewerId
    }
  }
}

    """;
  }

  String userById(String id) {
    return """
    query {
	userById (id: "${id}") {
		name
	}
}
    """;
  }

  String createReview(
      String title, String body, int rating, String userId, String movieId) {
    return """
mutation {
  createMovieReview(input: {
    movieReview: {
      title: "$title",
      body: "$body",
      rating: $rating,
      movieId: "$movieId",
      userReviewerId: "$userId"
    }})
  {
    movieReview {
      id
      title
      body
      rating
      userReviewerId
    }
  }
}
    """;
  }
}
