import 'dart:convert';

class Movie {
  String title;
  String releaseDate;
  String id;
  String directorId;

  Movie(
      {required this.title,
      required this.releaseDate,
      required this.id,
      required this.directorId});

  Movie copyWith({
    String? title,
    String? releaseDate,
    String? id,
    String? directorId,
  }) {
    return Movie(
        title: title ?? this.title,
        releaseDate: releaseDate ?? this.releaseDate,
        id: id ?? this.id,
        directorId: directorId ?? this.directorId);
  }

  Map<String, dynamic> toMap() {
    return {
      'title': title,
      'releaseDate': releaseDate,
      'id': id,
      'directorId': directorId,
    };
  }

  factory Movie.fromMap(Map<String, dynamic> map) {
    return Movie(
      title: map['title'] ?? '',
      releaseDate: map['releaseDate'] ?? '',
      id: map['id'] ?? '',
      directorId: map['movieDirectorId'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory Movie.fromJson(String source) => Movie.fromMap(json.decode(source));

  @override
  String toString() =>
      'Movie(title: $title, releaseDate: $releaseDate, id: $id, directorId: $directorId)';

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Movie &&
        other.title == title &&
        other.releaseDate == releaseDate &&
        other.id == id &&
        other.directorId == directorId;
  }

  @override
  int get hashCode =>
      title.hashCode ^ releaseDate.hashCode ^ id.hashCode ^ directorId.hashCode;
}
