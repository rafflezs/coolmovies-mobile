import 'dart:convert';

class Review {
  String body;
  String title;
  String id;
  int rating;
  String userReviewerId;
  Review({
    required this.body,
    required this.title,
    required this.id,
    required this.rating,
    required this.userReviewerId,
  });

  Review copyWith({
    String? body,
    String? title,
    String? id,
    int? rating,
    String? userReviewerId,
  }) {
    return Review(
      body: body ?? this.body,
      title: title ?? this.title,
      id: id ?? this.id,
      rating: rating ?? this.rating,
      userReviewerId: userReviewerId ?? this.userReviewerId,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'body': body,
      'title': title,
      'id': id,
      'rating': rating,
      'userReviewerId': userReviewerId,
    };
  }

  factory Review.fromMap(Map<String, dynamic> map) {
    print("\n\nReview map : " + map.toString() + "\n\n");
    return Review(
      body: map['body'] ?? '',
      title: map['title'] ?? '',
      id: map['id'] ?? '',
      rating: map['rating'] ?? 0,
      userReviewerId: map['userReviewerId'] ?? '',
    );
  }

  String toJson() => json.encode(toMap());

  factory Review.fromJson(String source) => Review.fromMap(json.decode(source));

  @override
  String toString() {
    return 'Review(body: $body, title: $title, id: ${id.toString}, rating: $rating, userReviewerId: $userReviewerId)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is Review &&
        other.body == body &&
        other.title == title &&
        other.id == id &&
        other.rating == rating &&
        other.userReviewerId == userReviewerId;
  }

  @override
  int get hashCode {
    return body.hashCode ^
        title.hashCode ^
        id.hashCode ^
        rating.hashCode ^
        userReviewerId.hashCode;
  }
}
