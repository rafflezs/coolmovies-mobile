import 'package:coolmovies/models/movie.dart';
import 'package:coolmovies/view/screens/movie_screen/movie_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class LatestMoviesCard extends StatefulWidget {
  Movie movie;
  LatestMoviesCard({required this.movie, Key? key}) : super(key: key);

  @override
  _LatestMoviesCardState createState() => _LatestMoviesCardState();
}

class _LatestMoviesCardState extends State<LatestMoviesCard> {
  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;
    return InkWell(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return MoviePageScreen(movie: widget.movie);
        }));
      },
      child: SizedBox(
        child: Column(
          children: [
            Text(
              widget.movie.title +
                  " (${DateTime.parse(widget.movie.releaseDate).year})",
              style: GoogleFonts.robotoSlab(
                fontSize: 18,
                color: Colors.black,
                fontWeight: FontWeight.w600,
              ),
              maxLines: 2,
            ),
            Hero(
              tag: 'dash',
              child: Image.asset(
                "assets/images/movies_poster_3.png",
                height: _screenSize.height * 0.4,
                fit: BoxFit.fill,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
