import 'package:card_swiper/card_swiper.dart';
import 'package:coolmovies/models/movie.dart';
import 'package:coolmovies/service/queries.dart';
import 'package:coolmovies/view/components/bottom_bar.dart';
import 'package:coolmovies/view/screens/home_screen/latest_movies_card.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:coolmovies/models/user.dart' as user;

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Movie> movies = [];

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(milliseconds: 0), _fetchLatestMovies);
  }

  @override
  Widget build(BuildContext context) {
    var _screenSize = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        elevation: 1,
        backgroundColor: Colors.white,
        title: Text(
          "Welcome back, ${user.user!.name}!",
          style: const TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.w400,
          ),
        ),
        centerTitle: true,
      ),
      bottomNavigationBar: CustomBoottomBar(pageIndex: 0),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(10),
              child: Text(
                "Latest Movies",
                style: GoogleFonts.roboto(
                  fontSize: 24,
                  color: Colors.black,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
            SizedBox(
              height: _screenSize.height * 0.44,
              child: Swiper(
                itemCount: movies.length,
                autoplay: true,
                duration: 1200,
                loop: true,
                curve: Curves.ease,
                itemBuilder: (context, index) => LatestMoviesCard(
                  movie: movies[index],
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(10),
              child: Text(
                "Your reviews",
                style: GoogleFonts.roboto(
                  fontSize: 24,
                  color: Colors.black,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
            Card(),
          ],
        ),
      ),
    );
  }

  void _fetchLatestMovies() async {
    var client = GraphQLProvider.of(context).value;
    final QueryResult result = await client.query(QueryOptions(
      document: gql(Queries().allMovies()),
    ));

    if (result.hasException) {
      Exception("Query failed!");
    }

    if (result.data != null) {
      var _moviesMap = result.data!["allMovies"]["edges"] as List;
      for (int i = 0; i < _moviesMap.length; i++) {
        setState(() {
          movies.add(Movie.fromMap(_moviesMap[i]["node"]));
        });
      }
    }
  }

  void _fetchReviews() async {
    var client = GraphQLProvider.of(context).value;
    final QueryResult result = await client.query(QueryOptions(
      document: gql(Queries().allMovies()),
    ));

    if (result.hasException) {
      Exception("Query failed!");
    }

    if (result.data != null) {
      var _moviesMap = result.data!["allMovies"]["edges"] as List;
      for (int i = 0; i < _moviesMap.length; i++) {
        setState(() {
          movies.add(Movie.fromMap(_moviesMap[i]["node"]));
        });
      }
    }
  }
}
