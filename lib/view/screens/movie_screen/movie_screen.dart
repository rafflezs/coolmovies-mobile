import 'package:coolmovies/models/movie.dart';
import 'package:coolmovies/models/review.dart';
import 'package:coolmovies/service/queries.dart';
import 'package:coolmovies/view/components/star_rating.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MoviePageScreen extends StatefulWidget {
  Movie movie;
  MoviePageScreen({required this.movie, Key? key}) : super(key: key);

  @override
  _MoviePageScreenState createState() => _MoviePageScreenState();
}

class _MoviePageScreenState extends State<MoviePageScreen> {
  var titleController = TextEditingController();
  var bodyController = TextEditingController();
  var intController = TextEditingController();
  int myRating = 0;

  String? directorName;
  List<Review> _reviews = [];

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(milliseconds: 0), _fetchDirector);
    Future.delayed(const Duration(milliseconds: 0), _fetchReviews);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        foregroundColor: Colors.black,
        elevation: 1,
        backgroundColor: Colors.white,
        title: Text(
          widget.movie.title +
              " (${DateTime.parse(widget.movie.releaseDate).year})",
          style: GoogleFonts.robotoSlab(
            fontSize: 18,
            fontWeight: FontWeight.w600,
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add_circle),
        onPressed: () {
          showDialog(
            context: context,
            useSafeArea: false,
            builder: (context) {
              return AlertDialog(
                backgroundColor: Colors.white,
                actionsAlignment: MainAxisAlignment.start,
                actions: [
                  InkWell(
                    onTap: () {
                      Navigator.of(context).pop();
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Icon(Icons.arrow_back),
                        SizedBox(width: 15),
                        Text(
                          "Cancel",
                          style: GoogleFonts.roboto(
                            fontSize: 18,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                      ],
                    ),
                  ),
                  Text(
                    "Title",
                    textAlign: TextAlign.start,
                    style: GoogleFonts.robotoSlab(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: TextFormField(
                      controller: titleController,
                    ),
                  ),
                  Divider(height: 40),
                  Text(
                    "Body",
                    textAlign: TextAlign.start,
                    style: GoogleFonts.robotoSlab(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: TextFormField(
                      controller: bodyController,
                      maxLines: 10,
                    ),
                  ),
                  Divider(height: 40),
                  Text(
                    "Rating",
                    textAlign: TextAlign.start,
                    style: GoogleFonts.robotoSlab(
                      fontSize: 18,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: StarRating(
                        rating: myRating,
                        onRatingChanged: (rating) =>
                            setState(() => myRating = rating)),
                  ),
                  Divider(height: 40),
                  ElevatedButton(
                    onPressed: () {
                      _createReview();
                      Navigator.of(context).pop();
                    },
                    child: Text("Publish Review"),
                  )
                ],
              );
            },
          );
        },
        backgroundColor: Colors.blue,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Hero(
              tag: 'dash',
              child: Image.asset("assets/images/movies_poster_3.png"),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                decoration: const BoxDecoration(
                  borderRadius: BorderRadius.all(
                    Radius.circular(5),
                  ),
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                        color: Colors.black45,
                        spreadRadius: 0.25,
                        offset: Offset(0, 5),
                        blurRadius: 5),
                  ],
                ),
                child: Column(
                  children: [
                    Text(
                      "Director: ${directorName}",
                      style: GoogleFonts.robotoSlab(
                        fontSize: 18,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    Text(
                      "Synopsis",
                      style: GoogleFonts.roboto(
                        fontSize: 15,
                        fontWeight: FontWeight.w800,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                          "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam erat felis, mattis blandit cursus ut, sodales sit amet magna.",
                          style: GoogleFonts.roboto(
                            fontSize: 15,
                            fontWeight: FontWeight.w600,
                          ),
                          textAlign: TextAlign.justify),
                    ),
                    const Divider(
                      color: Colors.black45,
                      height: 40,
                      thickness: 2,
                      indent: 20,
                      endIndent: 20,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10),
                      child: Text(
                        "User reviews",
                        style: GoogleFonts.roboto(
                          fontSize: 20,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                    ),
                    ListView.builder(
                      shrinkWrap: true,
                      itemCount: _reviews.length,
                      itemBuilder: (context, index) {
                        return Card(
                          elevation: 0,
                          color: Colors.transparent,
                          child: SizedBox(
                            width: 500,
                            child: Column(
                              children: [
                                Text(
                                  _reviews[index].title,
                                  style: const TextStyle(
                                    color: Colors.black,
                                  ),
                                ),
                                StarRating(
                                    rating: _reviews[index].rating,
                                    onRatingChanged: (rating) => setState(
                                        () => _reviews[index].rating = rating)),
                                Text(
                                  _reviews[index].body,
                                  style: const TextStyle(
                                    color: Colors.black,
                                  ),
                                  textAlign: TextAlign.justify,
                                ),
                                Text(
                                  "Review by: " +
                                      _reviews[index].userReviewerId,
                                  style: const TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                  textAlign: TextAlign.start,
                                ),
                                const Divider(
                                  color: Colors.black26,
                                  height: 40,
                                  thickness: 1,
                                  indent: 20,
                                  endIndent: 20,
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _fetchDirector() async {
    var client = GraphQLProvider.of(context).value;
    final QueryResult result = await client.query(QueryOptions(
      document: gql(Queries().movieDirectorById(widget.movie.directorId)),
    ));

    if (result.hasException) {
      Exception("Query failed!");
    }

    if (result.data != null) {
      setState(() {
        directorName = result.data!["data"]["movieDirectorById"];
      });
    }
  }

  void _fetchReviews() async {
    var client = GraphQLProvider.of(context).value;
    final QueryResult result = await client.query(QueryOptions(
      document: gql(Queries().reviewByMovieId(widget.movie.id)),
    ));

    if (result.hasException) {
      Exception("Query failed!");
    }

    if (result.data != null) {
      var _tempRepeviews = result.data!["allMovieReviews"]["nodes"] as List;
      for (int i = 0; i < _tempRepeviews.length; i++) {
        setState(() {
          _reviews.add(Review.fromMap(_tempRepeviews[i]));
        });
      }
    }
  }

  _createReview() async {
    SharedPreferences _sharedPreferences =
        await SharedPreferences.getInstance();
    var client = GraphQLProvider.of(context).value;
    final QueryResult result = await client.query(QueryOptions(
      document: gql(Queries().createReview(
          titleController.text,
          bodyController.text,
          myRating,
          _sharedPreferences.getString("token")!,
          widget.movie.id)),
    ));

    if (result.hasException) {
      Exception("Query failed!");
    }

    if (result.data != null) {
      setState(() {
        _reviews.add(
            Review.fromMap(result.data!["createMovieReview"]["movieReview"]));
      });
    }
  }
}
