import 'package:coolmovies/service/queries.dart';
import 'package:coolmovies/view/screens/home_screen/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:coolmovies/models/user.dart' as user;
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.transparent,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(15),
              child: Text(
                "Coolmovies",
                style: GoogleFonts.roboto(
                  fontSize: 36,
                  fontWeight: FontWeight.w900,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(5),
              child: Text(
                "For those who like movies\n...and those who dont!",
                style: GoogleFonts.roboto(
                  color: Colors.black54,
                  fontStyle: FontStyle.italic,
                  fontSize: 24,
                  fontWeight: FontWeight.w500,
                ),
                textAlign: TextAlign.center,
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 40),
              child: TextFormField(
                controller: _nameController,
                decoration: const InputDecoration(
                  label: Text("Name"),
                  floatingLabelBehavior: FloatingLabelBehavior.never,
                  suffixIcon: Icon(Icons.person),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(top: 40.0),
              child: ElevatedButton(
                onPressed: _login,
                child: const Text("Login"),
                style: ButtonStyle(
                  fixedSize: MaterialStateProperty.all(
                    const Size(160, 30),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _login() async {
    var client = GraphQLProvider.of(context).value;
    final QueryResult result = await client.query(
      QueryOptions(
        document: gql(Queries().createUser(_nameController.text)),
      ),
    );

    if (result.hasException) {
      Exception("Something wrong happened!");
    }

    if (result.data != null) {
      var _newUser = result.data!["createUser"]["user"];
      user.user = user.User.fromMap(_newUser);
      SharedPreferences _sharedPreferences =
          await SharedPreferences.getInstance();
      _sharedPreferences.setString("token", _newUser["id"]);
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => const HomeScreen()));
    }
  }
}
