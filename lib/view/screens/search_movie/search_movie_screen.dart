import 'package:coolmovies/models/movie.dart';
import 'package:coolmovies/service/queries.dart';
import 'package:coolmovies/view/components/bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:graphql_flutter/graphql_flutter.dart';

class MovieSearchScreen extends StatefulWidget {
  const MovieSearchScreen({Key? key}) : super(key: key);

  @override
  _MovieSearchScreenState createState() => _MovieSearchScreenState();
}

class _MovieSearchScreenState extends State<MovieSearchScreen> {
  List<Movie> movies = [];
  var _searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(milliseconds: 0), _fetchLatestMovies);
  }

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      ListView.builder(
        shrinkWrap: true,
        itemCount: movies.length,
        itemBuilder: (context, index) {
          return Container(
            child: Row(
              children: [
                Image.asset("assets/images/movies_poster_2.png"),
                Column(
                  children: [
                    Text(
                      movies[index].title +
                          " (${DateTime.parse(movies[index].releaseDate).year})",
                      style: GoogleFonts.robotoSlab(
                        fontSize: 18,
                        color: Colors.black,
                        fontWeight: FontWeight.w600,
                      ),
                      maxLines: 2,
                    ),
                  ],
                )
              ],
            ),
          );
        },
      ),
    ]);
  }

  void _fetchLatestMovies() async {
    var client = GraphQLProvider.of(context).value;
    final QueryResult result = await client.query(QueryOptions(
      document: gql(Queries().allMovies()),
    ));

    if (result.hasException) {
      Exception("Query failed!");
    }

    if (result.data != null) {
      var _moviesMap = result.data!["allMovies"]["edges"] as List;
      for (int i = 0; i < _moviesMap.length; i++) {
        setState(() {
          movies.add(Movie.fromMap(_moviesMap[i]["node"]));
        });
      }
    }
  }
}
