import 'package:flutter/material.dart';

class CustomBoottomBar extends StatefulWidget {
  int pageIndex;
  CustomBoottomBar({required this.pageIndex, Key? key}) : super(key: key);

  @override
  _CustomBoottomBarState createState() => _CustomBoottomBarState();
}

class _CustomBoottomBarState extends State<CustomBoottomBar> {
  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      currentIndex: widget.pageIndex,
      onTap: (index) => setState(() => widget.pageIndex = index),
      selectedItemColor: Colors.blue,
      unselectedItemColor: Colors.black45,
      items: const [
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: "Home",
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.search_rounded),
          label: "Search",
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.format_list_numbered_sharp),
          label: "Movie Ranks",
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.settings),
          label: "Settings",
        ),
      ],
    );
  }
}
