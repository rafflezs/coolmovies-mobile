import 'package:flutter/material.dart';

typedef RatingChangeCallback = void Function(int rating);

class StarRating extends StatelessWidget {
  int rating;
  final RatingChangeCallback onRatingChanged;

  StarRating({
    Key? key,
    required this.rating,
    required this.onRatingChanged,
  }) : super(key: key);

  Widget buildStar(BuildContext context, int index) {
    Icon icon;
    if (index >= rating) {
      icon = const Icon(
        Icons.star_border,
        color: Colors.amber,
      );
    } else if (index > rating - 1 && index < rating) {
      icon = const Icon(
        Icons.star_half,
        color: Colors.amber,
      );
    } else {
      icon = const Icon(
        Icons.star,
        color: Colors.amber,
      );
    }
    return InkResponse(
      onTap: onRatingChanged == null ? null : () => onRatingChanged(index + 1),
      child: icon,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
        children: List.generate(5, (index) => buildStar(context, index)));
  }
}
