import 'package:flutter/material.dart';
import 'package:coolmovies/models/user.dart' as user;

class CustomAppBar extends StatefulWidget {
  const CustomAppBar({Key? key}) : super(key: key);

  @override
  _CustomAppBarState createState() => _CustomAppBarState();
}

class _CustomAppBarState extends State<CustomAppBar> {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 1,
      backgroundColor: Colors.white,
      title: Text("Welcome back, ${user.user!.name}!"),
      centerTitle: true,
    );
  }
}
